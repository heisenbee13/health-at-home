import React from 'react';
//import useAxios from 'axios-hooks'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Home } from './Home'
import { About } from './About'
import { Recipes } from './Recipes1'
import { Workouts1 } from './Workouts'
import { HealthNews } from './HealthNews'
import { NoMatch } from './NoMatch'
import {Layout} from './Components/Layout'
import {NavigationBar} from './Components/NavigationBar'
import { Jumbotron } from './Components/Backgroundpic'
import {BicepCurls} from './WorkoutsPages/BicepCurls'
import {Dips} from './WorkoutsPages/Dips'
import {Situps} from "./WorkoutsPages/Sit-ups"
import {BlueberryRhubarbPie} from "./RecipesPages/BlueberryRhubarbPie"
import {RiceAndBeans} from "./RecipesPages/RiceAndBeans"
import {SpicyBEPCurry} from "./RecipesPages/SpicyBEPCurry"
import { TempArticle1, TempArticle2, TempArticle3 } from './Components/HealthNews/TempArticles';
import {WorkoutInstance} from './WorkoutInstance'
import {Article} from "./Components/HealthNews/Article";
import {RecipeInstance} from './RecipeInstances'

function App() {

//const[{data}] = useAxios("/api/allWorkouts")

  return (
    <React.Fragment>
      <NavigationBar/>
      <Jumbotron/>
      <Layout>
        <Router>
          <Switch>
            <Route exact path ="/" component = {Home} />
            <Route path ="/about" component = {About} />
            <Route exact path ="/recipes" component = {Recipes} />
            <Route path ="/workouts" component = {Workouts1} />
            {/* //<Route exact path ="/workouts1" component = {Workouts1} /> */}
            <Route exact path = "/workouts1/:id" component = {WorkoutInstance} />
            <Route exact path ="/recipes/:id" component = {RecipeInstance} />

            <Route exact path ="/healthnews" component = {HealthNews} />
            <Route exact path = "/healthnews/:id" component = {Article} />

            <Route path ="/bicepcurls" component = {BicepCurls} />
            <Route path ="/dips" component = {Dips} />
            <Route path ="/situps" component = {Situps} />
            <Route path ="/blueberryrhubarbpie" component = {BlueberryRhubarbPie} />
            <Route path ="/RiceAndBeans" component = {RiceAndBeans} />
            <Route path ="/SpicyBEPCurry" component = {SpicyBEPCurry} />
            <Route component = {NoMatch} />
          </Switch>
        </Router>
      </Layout>
    </React.Fragment>
  );
}

export default App;
