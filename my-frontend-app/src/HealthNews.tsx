import React, {Component} from 'react'
import { Button, Image, Col, Container, Row } from 'react-bootstrap'
import axios, { AxiosRequestConfig, AxiosPromise, AxiosResponse } from 'axios';
import {Pagination} from './Components/Pagination';
import { ArticleTeaser } from './Components/HealthNews/ArticleTeaser'
import ButtonToolbar from 'react-bootstrap/esm/ButtonToolbar'
import { Link } from 'react-router-dom'

export class HealthNews extends Component {
    state = {
        articles: [], 
        loading: false, 
        currentPage: 1, 
        articlesPerPage: 20
    };

    getArticles = async () => {
        this.setState({loading: true});
        const results = await axios.get('https://api.healthathome.me/api/healthnews?page='.concat(this.state.currentPage.toString()));
        this.setState({articles: results.data, loading: false});
    };

    componentDidMount() {
        
        this.getArticles();
    }

    render() {
        const { currentPage, articlesPerPage, articles, loading} = this.state;;
        const paginate = pageNum => this.setState({currentPage: pageNum})
        const nextPage = () => {
            this.state.currentPage = this.state.currentPage+1;
            this.getArticles();
        };
        const prevPage = () => {
            if (this.state.currentPage!=1)
                this.state.currentPage = this.state.currentPage-1;
            this.getArticles();
        };

        return (
            <div>
                <Container>
                    <h2>Health News</h2>
                    <h4>Get the latest health news to stay up to date with the best health practices and information!</h4>
                    <h4>Additional health resources:</h4>
                    <p>Nutrition/Food Resources:</p>
                    <ul>
                        <li>https://www.nutrition.gov/topics/whats-food</li>
                        <li>https://health.gov/our-work/food-nutrition</li>
                        <li>https://foodandnutrition.org/</li>
                    </ul>
                    <p>Workout Resources:</p>
                    <ul>
                        <li>https://www.hhs.gov/fitness/resource-center/physical-activity-resources/index.html</li>
                    </ul>
                    <p>General Health Resources:</p>
                    <ul>
                        <li>https://www.usa.gov/health-resources</li>
                        <li>https://www.texashealth.org/en</li>
                    </ul>
                    <hr/>
                    <Row className="justify-content-center">
                        {/* Health News title row */}
                    
                        
                        <Pagination currentPage={currentPage} nextPage={nextPage} prevPage={prevPage} />
                        <ArticleTeaser articles = {articles} loading={loading}/>
                        <Pagination currentPage={currentPage} nextPage={nextPage} prevPage={prevPage} />
                    </Row>
                </Container>
            </div>
        )
    }

}


export const HealthNewsOld = () => (
    <div>
        <Container>
            {/* Health News title row */}
        
            <h2>Health News</h2>
            <h4>Get the latest health news to stay up to date with the best health practices and information!</h4>
            <hr/>
        

            {/* Adding another row with vertical padding and internal components centered */}
            <Row className="my-3 justify-content-center">
                <Col lg = {4} md = {6} className="p-3">
                    <Container className="bg-light border border-dark rounded-lg p-3">
                        <h5>KTVQ Billings News</h5>
                        <Image className="my-1" src="https://ewscripps.brightspotcdn.com/dims4/default/11546a2/2147483647/strip/true/crop/889x467+0+17/resize/1200x630!/quality/90/?url=https%3A%2F%2Fewscripps.brightspotcdn.com%2F07%2F1a%2F8bf8ee874bc189c1288d62236bb7%2Fcovid-19-cases.png" fluid/>
                        <p>10 infected, 3 hospitalized in Columbus assisted living COVID-19 outbreak - KTVQ Billings News</p>
                        <Button variant="Primary" className="bg-primary text-white" href="/healthnews/article1">More...</Button>
                    </Container>
                
                </Col>
                <Col lg = {4} md = {6} className="p-3">
                    <Container className="bg-light border border-dark rounded-lg p-3">
                        <h5>WDIV ClickOnDetroit</h5>
                        <Image className="my-1" src="https://www.clickondetroit.com/resizer/RCYIDewlHXXjIijw-3FzWNw4oPc=/1600x1185/smart/filters:format(jpeg):strip_exif(true):strip_icc(true):no_upscale(true):quality(65)/cloudfront-us-east-1.images.arcpublishing.com/gmg/3MKBVPNUH5BSNAH2KCQO66EO4U.jpg" fluid/>
                        <p>2 Livingston County locations listed as potential coronavirus (COVID-19) exposure sites - WDIV ClickOnDetroit</p>
                        <Button variant="Primary" className="bg-primary text-white" href="/healthnews/article2">More...</Button>
                    </Container>
                
                </Col>
                <Col lg = {4} md = {6} className="p-3">
                    <Container className="bg-light border border-dark rounded-lg p-3">
                        <h5>News On 6</h5>
                        <Image className="my-1" src="https://hot-town-images.s3.amazonaws.com/kwtv/production/2020/June/18/coronavirus-covid19.1592495430545.jpeg" fluid/>
                        <p>1,025 New Confirmed COVID-19 Cases, 11 More Virus-Related Deaths Reported In State, Health Officials Say - News On 6</p>
                        <Button variant="Primary" className="bg-primary text-white" href="/healthnews/article3">More...</Button>
                    </Container>
                </Col>
            
            </Row>
        </Container>

        <hr></hr>
        <hr></hr>
        <Link to= '/recipes'> 
            <ButtonToolbar>
                <Button variant='light' size='sm' block>
                    Go to the Recipes page
                </Button>
            </ButtonToolbar>
        </Link>
            
        <Link to= '/workouts'> 
            <ButtonToolbar>
                <Button variant='light' size='sm' block>
                    Go to the Workouts page
                </Button>
            </ButtonToolbar>
        </Link>
    </div>

)