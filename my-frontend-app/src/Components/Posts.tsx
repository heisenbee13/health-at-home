import React, {Component} from 'react'
import { Button, ButtonToolbar, Col, Container, Nav, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export const Posts = ({ posts, loading }) => {
    if (loading) {
        return <h2>Loading...</h2>
    }
    


    return (
    <Row className="my-3">
        <Col xs={12} className = "text-center">
        </Col>
        {posts.map(post => (
            <Col lg={4} md={6} className="p-2"  key={post.name}>
                <Container className = "p-1 bg-light border border-dark rounded-lg">
                    <h4> {post.name}</h4>
                    
                   
                    <Button href = {String("/workouts1/").concat(post.id)}>Find out more</Button>

                </Container>
            </Col>
        ))}
    </Row>



    )


}