import React, {Component} from 'react'
import { Col } from 'react-bootstrap'

export const Pagination = ({ currentPage, nextPage, prevPage}) => {


    return (
        <Col xs={12} className="justify-content-center">
            <nav>
                <ul className="pagination justify-countent-center">
                    <li className="page-item">
                        <a className="page-link" href="#" onClick={() => prevPage()}>Previous</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#" >{currentPage}</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#" onClick={() => nextPage()}>Next</a>
                    </li>
                </ul>
            </nav>
        </Col>
    )
}




// import React, {Component} from 'react'

// export const Pagination = ({ postsPerPage, totalPosts, paginate, nextPage, prevPage}) => {
//     const pageNumbers: number [] = [];
//     for(let i = 1; i <= Math.ceil(totalPosts/postsPerPage); i++) {
//         pageNumbers.push(i);
//     }

//     return (
//         <nav>
//             <ul className="pagination justify-countent-center">
//                 <li className="page-item">
//                     <a className="page-link" href="#" onClick={() => prevPage()}>Previous</a>
//                 </li>
//                 {pageNumbers.map(num => (
//                     <li className="page-item" key={num}>
//                         <a onClick={() => paginate(num)} href="#" className="page-link">{num}</a>
//                     </li>
//                 ))}
//                 <li className="page-item">
//                     <a className="page-link" href="#" onClick={() => nextPage()}>Next</a>
//                 </li>
//             </ul>
//         </nav>
//     )
// }