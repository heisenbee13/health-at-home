import React, {Component} from 'react'
import { Col } from 'react-bootstrap'

export const Pagination = ({ currentPage, nextPage, prevPage}) => {


    return (
        <Col xs={12} className="justify-content-center">
            <nav>
                <ul className="pagination justify-countent-center">
                    <li className="page-item">
                        <a className="page-link" href="#" onClick={() => prevPage()}>Previous</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#" >{currentPage}</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#" onClick={() => nextPage()}>Next</a>
                    </li>
                </ul>
            </nav>
        </Col>
    )
}
