import React from 'react';
import Container from "react-bootstrap/Container"

/**
 * This is explained in the video, but I think this allows all the words/paragraphs 
 * in all the pages to be confined to a container. 
 * Without this, the words would stretch to fit the entire screen and looks a little messy. 
 * I think this is the convention/syntax to carry out the same layout throughout these pages.
 */

export const Layout = (props: { children: React.ReactNode; }) => (
    <Container>
        {props.children}
    </Container>
)