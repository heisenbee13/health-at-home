import React, {Component} from 'react'
import { Button, ButtonToolbar, Col, Container, Row, Table} from 'react-bootstrap'
import { Link } from 'react-router-dom'

export const Posts_Recipes = ({ posts}) => {


    return (
    <Table striped bordered hover>
        <thead>
            <tr>
            <th>Recipe</th>
            <th>Cuisines</th>
            <th>Diets</th>
            <th>Prep Time (min)</th>
            <th>Source URL</th>
            </tr>
        </thead>
        <tbody>
            {posts.map(post => (
                <tr key = {post.id}>
                    <td><a href= {String("/recipes/").concat(post.id)}> {post.title} </a></td>
                    <td>{post.cuisines}</td>
                    <td>{post.diets}</td>
                    <td>{post.readyInMinutes}</td>
                    <td><Link to= {post.sourceUrl}>{post.sourceUrl}</Link></td>
                </tr>
            ))}
        </tbody>
    </Table>
    )


}