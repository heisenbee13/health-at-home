import React from "react"
import { Container, Image, Row, Col, Button } from "react-bootstrap"

export const TempArticle1 = () => (
    <Container className="mb-5">
        <h3>I Tried Powder With Over 20 Grams of Protein - and the Special Ingredient Was Crickets</h3>
        <small>Samantha Brodsky</small>
        <em className="mx-2">-</em>
        <small>POPSUGAR</small> <br/>
        <small>2020-09-30</small>
        <hr/>
        <Row className="justify-content-center my-3">
            <Col lg={6}>
                <Image src="https://media1.popsugar-assets.com/files/thumbor/gI2nbDN04GzkH_ZYzjK15XPSOBw/0x671:4028x2786/fit-in/1200x630/filters:format_auto-!!-:strip_icc-!!-/2020/09/29/848/n/1922729/1aa44ad95f738926647c26.14979977_.jpg" fluid />
            </Col>
        </Row>

        <p>Image Source: Getty / picture alliance<br/><br/>I know that people eat insects as part of their diet, but I'm not one for putting anything even remotely insect-related close to my tastebuds. Cricket powder, though, does seem like a less-intimidating way to consume…</p>
        <Button variant="Primary" className="bg-primary text-white" href="https://www.popsugar.com/fitness/cricket-protein-powder-review-47833921">View this article</Button>
        
    </Container>
)

export const TempArticle2 = () => (
    <Container className="mb-5">
        <h3>Potatoes: Type matters</h3>
        <small>everybodysfit</small>
        <em className="mx-2">-</em>
        <small>Naturalnewsblogs.com</small> <br/>
        <small>2020-09-30</small>
        <hr/>
        <Row className="justify-content-center my-3">
            <Col lg={6}>
                <Image src="https://naturalnewsblogs.com/wp-content/uploads/2015/03/Sweet-Potatoes.jpg" fluid />
            </Col>
        </Row>

        <p>White, red, and sweet…. they make up the potato family. These starchy vegetables are staple of our diet, but each differ in taste, size, and nutrients. The potato is economical, can be prepared and used for so many different varieties of food, and can either …</p>
        <Button variant="Primary" className="bg-primary text-white" href="https://naturalnewsblogs.com/potatoes-type-matters/">View this article</Button>
        
    </Container>
)

export const TempArticle3 = () => (
    <Container className="mb-5">
        <h3>Association between esodeviation and primary open-angle glaucoma: the 2010-2011 Korea National Health and Nutrition Examination Survey</h3>
        <small>Kim, J.-s., Kim, Y. K., Kim, Y. W., Baek, S. U., Ha, A., Lee, J., Lee, H.-J., Kim, D. W., Jeoung, J. W., Kim, S.-J., Park, K. H.</small>
        <em className="mx-2">-</em>
        <small>The BMJ</small> <br/>
        <small>2020-09-30</small>
        <hr/>
        <Row className="justify-content-center my-3">
            <Col lg={6}>
                <Image src="https://bjo.bmj.com/sites/default/files/highwire/bjophthalmol/104/10.cover-source.jpg" fluid />
            </Col>
        </Row>

        <p>Background/Aims<br/>To evaluate the association between strabismus and primary open-angle glaucoma (POAG) in a representative Korean population.<br/>Methods<br/>A total of 11 114 participants aged 20 years or older in the Korea National Health and Nutrition Examinatio…</p>
        <Button variant="Primary" className="bg-primary text-white" href="https://bjo.bmj.com/content/early/2020/09/30/bjophthalmol-2020-316901">View this article</Button>
        
    </Container>
)