import React from 'react'
import {Row, Col, Card, CardDeck, Button} from 'react-bootstrap'

export const ArticleTeaser = ({articles, loading}) => {
    if (loading) {
        return <h2>Loading...</h2>
    }



    return (
        <CardDeck>
            <Row>
                {articles.map(article => (
                    <Col md={4} xs={12} className="my-3">
                        <Card key={article.id}>
                            <Card.Img variant="top" src={article.urlToImage} alt={article.urlToImage}/>
                            <Card.Body>
                                <Card.Title>{article.title}</Card.Title>
                                <Card.Subtitle>{article.author} - {article.source}</Card.Subtitle>
                                <Card.Text>{article.description}</Card.Text>
                                <Button href={String("/healthnews/").concat(article.id)}>Check out more...</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                ))}
                
            </Row>
        </CardDeck>
    )
}