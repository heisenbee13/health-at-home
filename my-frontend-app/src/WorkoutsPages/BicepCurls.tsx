import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'

export const BicepCurls = () => (
    <div>
        <h1>Bicep Curls With Dumbells</h1>
        <hr></hr>
        <Container>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Category: Arms</p>
                </Col>
                <Col md = {3}>
                    <p>Benefits/Muscles Worked: Biceps brachii</p>
                </Col>
                <Col md = {3}>
                    <p>Secondary muscles: Brachialis</p>
                </Col>
            </Row>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Equipment needed: Barbell</p>
                </Col>
                <Col md = {3}>
                    <p>Description:
                    Hold the Barbell shoulder-wide, the back is straight, the shoulders slightly back, 
                    the arms are streched. Bend the arms, bringing the weight up, with a fast movement. 
                    Without pausing, let down the bar with a slow and controlled movement.
                    Don't allow your body to swing during the exercise, all work is done by the biceps, 
                    which are the only mucles that should move (pay attention to the elbows).</p>
                </Col>
                <Col>
                    <Image src="https://wger.de/media/exercise-images/74/Bicep-curls-1.png" fluid />
                </Col>
            </Row>
        </Container>
    </div>

)
