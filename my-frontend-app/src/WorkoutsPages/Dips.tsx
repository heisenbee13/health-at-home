import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'

export const Dips = () => (
    <div>
        <h1>Dips</h1>
        <hr></hr>
        <Container>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Category: Arms</p>
                </Col>
                <Col md = {3}>
                    <p>Benefits/Muscles Worked: Triceps brachii</p>
                </Col>
                <Col md = {3}>
                    <p>Secondary muscles: Pectoralis major, Rectus abdominis</p>
                </Col>
            </Row>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Equipment needed: N/A</p>
                </Col>
                <Col md = {3}>
                    <p>Description:
                        Hold onto the bars at a narrow place (if they are not parallel) and press yourself up, 
                        but don't stretch the arms completely, so the muscles stay during the whole exercise 
                        under tension. Now bend the arms and go down as much as you can, keeping the elbows always 
                        pointing back, At this point, you can make a short pause before repeating the movement.</p>
                </Col>
                <Col>
                    <Image src="https://wger.de/media/exercise-images/82/Tricep-dips-2-2.png" fluid />
                </Col>
            </Row>
        </Container>
    </div>

)