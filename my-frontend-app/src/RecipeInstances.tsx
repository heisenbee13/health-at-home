import React, {Component} from 'react'
import { Button, ButtonToolbar, Col, Container, Nav, Row, Image} from 'react-bootstrap'
import { Link, useParams, RouteComponentProps } from 'react-router-dom'
import axios, { AxiosRequestConfig, AxiosPromise, AxiosResponse } from 'axios';
import { userInfo } from 'os';

export class RecipeInstance extends Component {
    state = {
        info: {
            name: "",
            cuisines: "",
            diets: "",
            summary: "",
            image: "",
        },
        loading: false,
    };
    
    id;
    constructor(props: RouteComponentProps<{id}> ) {
        super(props);
        const {id} = props.match.params;
        this.id = id;
    }
    componentDidMount() {
        const getPosts = async () => {
            {this.setState({loading: true})};
            const results = await axios.get('https://api.healthathome.me/api/recipeInfo?id='.concat((this.id)));
            this.setState({info: results.data, loading: true});
        };
        getPosts();
    }
    render() {
        const {info, loading} = this.state;
        console.log(info);

        return (
            <Row className="my-3">
            <Col xs={12} className = "text-center">
            </Col>
            
                <Col lg={4} md={6} className="p-2">
                    <Container className = "p-1 bg-light border border-dark rounded-lg">
                        <h4>{info.name}</h4>
                        <p>Cuisines: {info.cuisines}</p>
                        <p>Diets: {info.diets}</p>
                        <p>Summary: {info.summary}</p>
                        <Image src={info.image} fluid />
                        
                    </Container>
                </Col>
           
            </Row>

    )
    }


}
