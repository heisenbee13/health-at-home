import React, {Component} from 'react'
import { Button, ButtonToolbar, Col, Container, Nav, Row, Image } from 'react-bootstrap'
import { Link, RouteComponentProps } from 'react-router-dom'
import axios, { AxiosRequestConfig, AxiosPromise, AxiosResponse } from 'axios';
import { userInfo } from 'os';


const categoryIDToWord : {[key: number]: string} = {
    10: "Abs",
    8: "Arms",
    12: "Back",
    14: "Calves",
    11: "Chest",
    9: "Legs",
    13: "Shoulders",
}

const musclesIDToWord : {[key: number]: string} = {
    2: "Anterior deltoid",
    1: "Biceps brachii",
    11: "Biceps femoris",
    13: "Brachialis",
    7: "Gastrocnemius",
    8: "Gluteus maximus",
    12: "Latissimus dorsi",
    14: "Obliquus externus abdominus",
    4: "Pectoralis major",
    10: "Quadriceps femoris",
    6: "Rectus abdominis",
    3: "Serratus anterior",
    15: "Soleus",
    9: "Trapezius",
    5: "Triceps brachii"
}

const equipmentIDToWord : {[key: number]: string} = {
    1: "Barbell",
    8: "Bench",
    3: "Dumbell",
    4: "Gym mat",
    9: "Incline bench",
    10: "Kettlebell",
    7: "none (bodyweight exercise)",
    6: "Pull-up bar",
    5: "Swiss Ball",
    2: "SZ-Bar",
}


export class WorkoutInstance extends Component {
    state = {
        info: {
            name: "",
            description: "",
            category: "",
            primary_muscle: "",
            secondary_muscle: "",
            equipment: "",
            image: "",
        },
        loading: false,
    };
    
    id;
    constructor(props: RouteComponentProps<{id}> ) {
        super(props);
        const {id} = props.match.params;
        this.id = id;
    }
    componentDidMount() {
        const getPosts = async () => {
            {this.setState({loading: true})};
            const results = await axios.get("https://api.healthathome.me/api/workoutInfo?id=".concat((this.id)));
            const tempData = results.data;
            if (tempData.exercise_images === undefined || tempData.exercise_images.length == 0) {
                var workout_info = {
                    name: tempData.name,
                    description: tempData.description,
                    category: tempData.category,
                    primary_muscle: tempData.muscles[0],
                    secondary_muscle: tempData.muscles_secondary[0],
                    equipment: tempData.equipment[0],
                    image: "https://images.pexels.com/photos/209969/pexels-photo-209969.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",
                }
                console.log(workout_info);
                this.setState({info: workout_info, loading: false});
            } else {
                workout_info = {
                    
                    name: tempData.name,
                    description: tempData.description,
                    category: tempData.category,
                    primary_muscle: tempData.muscles[0],
                    secondary_muscle: tempData.muscles_secondary[0],
                    equipment: tempData.equipment[0],
                    image: tempData.exercise_images[1].image,

                };
            
                console.log(workout_info);
                this.setState({info: workout_info, loading: false});
            }
        };
        getPosts();
    }
    render() {
        const {info, loading} = this.state;
        var cat = categoryIDToWord[info.category];
        var firstMuscleName = musclesIDToWord[info.primary_muscle];
        var secondMuscleName = musclesIDToWord[info.secondary_muscle];
        var equipmentName = equipmentIDToWord[info.equipment];

        return (
            <Row className="my-3">
            <Col xs={12} className = "text-center">
            </Col>
            
                <Col lg={4} md={6} className="p-2">
                    <Container className = "p-1 bg-light border border-dark rounded-lg">
                        <h4> {info.name}</h4>
                        <hr></hr>
                        <p>Description: {info.description}</p>
                        <p>Category: {cat} </p>
                        <p>Equipment: {equipmentName}</p>
                        <p>Muscles Primary: {firstMuscleName}</p>
                        <p>Muscles Secondary: {secondMuscleName}</p>
                        <Row className="justify-content-center my-3">
                            <Col lg={6}>
                                <Image src={info.image} fluid />
                            </Col>
                        </Row>

                        
                    </Container>
                </Col>
           
            </Row>

    )
    }


}