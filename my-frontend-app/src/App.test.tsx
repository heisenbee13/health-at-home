import React from 'react';
import { render } from '@testing-library/react';
import {Home} from "./Home";
import {About} from "./About"
import {Recipes} from "./Recipes";
import {HealthNews} from "./HealthNews";
import {Workouts1} from "./Workouts";
import {BicepCurls} from "./WorkoutsPages/BicepCurls"
import {RiceAndBeans} from "./RecipesPages/RiceAndBeans"


// Testing Home Page renders
test("renders Home page", () => {
  const component = render(<Home />);
  const element = component.getByText("EMBRACE a healthy lifestyle today");
  expect(element).toBeInTheDocument();
});

// Testing About Page renders
test("renders About page", () => {
  const component = render(<About />);
  const element = component.getByText("Our Mission");
  expect(element).toBeInTheDocument();
});

// Testing Workouts renders
test("renders Workouts page", () => {
  const component = render(<Workouts1 />);
  const element = component.getByText("Workouts");
  expect(element).toBeInTheDocument();
});

// Testing BicepCurls renders
test("renders BicepCurls page", () => {
  const component = render(<BicepCurls />);
  const element = component.getByText("Bicep Curls With Dumbells");
  expect(element).toBeInTheDocument();
});

// Testing RiceandBeans page renders
 test("renders RiceandBeans page", () => {
  const component = render(<RiceAndBeans />);
  const element = component.getByText("Easy Homemade Rice and Beans");
  expect(element).toBeInTheDocument();
}); 

/*
// Testing HealthNews renders
test("renders HealthNews page", () => {
  const component = render(<HealthNews />);
  const element = component.getByText("Get the latest health news");
  expect(element).toBeInTheDocument();
});
*/