import React from 'react';
import Table from 'react-bootstrap/Table';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button'
import ButtonToolbar from 'react-bootstrap/esm/ButtonToolbar'

export const Recipes = () => (
  <div>
    <h1>Recipes</h1>
    <hr></hr>
        <h4>Take your pick of recipes that are not only healthy but also DELICIOUS.</h4>
        <p>Find foods that you love, guilt-free snacking guaranteed!</p>
    <hr></hr>
​
  <Table striped bordered hover>
    <thead>
      <tr>
        <th>Recipe</th>
        <th>Calories (per serving)</th>
        <th>Primary Ingredients</th>
        <th>Prep Time (min)</th>
        <th>Price per Serving (USD)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><Link to= '/blueberryrhubarbpie'> Blueberry Rhubarb Pie </Link></td>
        <td>317</td>
        <td>Rhubarb, Bluberries (or Blackberries), Tapioca</td>
        <td>60</td>
        <td>$1.04</td>
      </tr>
      <tr>
        <td><Link to= '/RiceAndBeans'>Homemade Rice and Beans </Link></td>
        <td>446</td>
        <td>Black beans, Rotel tomatoes, Chili Powder</td>
        <td>35</td>
        <td>$1.06</td>
      </tr>
      <tr>
        <td><Link to= '/SpicyBEPCurry'>Spicy Black-Eyed Pea Curry</Link></td>
        <td>130</td>
        <td>Black-eyed peas, Italian eggplant, Swiss chard</td>
        <td>45</td>
        <td>$1.07</td>
      </tr>
    </tbody>
  </Table>
  <hr></hr>
  <hr></hr>
  <Link to= '/workouts'> 
    <ButtonToolbar>
      <Button variant='light' size='sm' block>
        Go to the Workouts page
      </Button>
    </ButtonToolbar>
  </Link>
        
  <Link to= '/healthnews'> 
    <ButtonToolbar>
      <Button variant='light' size='sm' block>
        Go to the Health News page
      </Button>
    </ButtonToolbar>
  </Link>
​
</div>
)