import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'

export const RiceAndBeans = () => (
    <div>
        <h1>Easy Homemade Rice and Beans</h1>
        <hr></hr>
        <Container>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Diet Options: Vegetarian, Vegan, Gluten-Free,
                    Dairy-Free</p>
                </Col>
                <Col md = {3}>
                    <p>Spoonacular Score: 98.0</p>
                </Col>
                <Col md = {3}>
                    <p>Health Score: 61.0</p>
                </Col>
            </Row>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Servings: 2</p>
                </Col>
                <Col md = {3}>
                    <p>Summary:
                    Need a gluten free and vegan main course? Easy Homemade Rice and Beans could
                    be a great recipe to try. One serving contains 446 calories, 19g of protein, and
                    4g of fat. For $1.06 per serving, this recipe covers 26% of your daily requirements
                    of vitamins and minerals. 471 person have made this recipe and would make it again.
                    A mixture of olive oil, ground pepper, onion, and a handful of other ingredients are
                    all it takes to make this recipe so yummy. To use up the olive oil you could follow
                    this main course with the  as a dessert. From preparation to the plate, this recipe
                    takes approximately 35 minutes. All things considered, we decided this recipe
                    deserves a spoonacular score of 99%. This score is awesome.
                    </p>
                </Col>
                <Col>
                    <Image src="https://spoonacular.com/recipeImages/716627-556x370.jpg" fluid />
                </Col>
            </Row>
        </Container>
    </div>

)
