import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'

export const SpicyBEPCurry = () => (
    <div>
        <h1>Spicy Black-Eyed Peas Curry with Swiss Chard and Roasted Eggplant</h1>
        <hr></hr>
        <Container>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Diet Options: Vegetarian, Vegan, Gluten-Free,
                    Dairy-Free</p>
                </Col>
                <Col md = {3}>
                    <p>Spoonacular Score: 98.0</p>
                </Col>
                <Col md = {3}>
                    <p>Health Score: 87.0</p>
                </Col>
            </Row>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Servings: 6</p>
                </Col>
                <Col md = {3}>
                    <p>Summary:
                    The recipe Spicy Black-Eyed Pea Curry with Swiss Chard and Roasted Eggplant is ready
                    in roughly 45 minutes and is definitely a spectacular gluten free and vegan
                    option for lovers of Indian food. This recipe makes 6 servings with 130 calories,
                    7g of protein, and 2g of fat each. For 99 cents per serving, this recipe covers 21%
                    of your daily requirements of vitamins and minerals. A few people made this recipe,
                    and 32 would say it hit the spot. A mixture of chilies, olive oil, garam masala, and
                    a handful of other ingredients are all it takes to make this recipe so delicious. All
                    things considered, we decided this recipe deserves a spoonacular score of 98%. This
                    score is amazing. </p>
                </Col>
                <Col>
                    <Image src="https://spoonacular.com/recipeImages/798400-556x370.jpg" fluid />
                </Col>
            </Row>
        </Container>
    </div>

)
