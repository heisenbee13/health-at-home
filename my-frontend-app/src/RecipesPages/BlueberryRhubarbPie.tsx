import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'

export const BlueberryRhubarbPie = () => (
    <div>
        <h1>Blueberry Rhubarb Pie</h1>
        <hr></hr>
        <Container>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Diet Options: Vegetarian</p>
                </Col>
                <Col md = {3}>
                    <p>Spoonacular Score: 15.0</p>
                </Col>
                <Col md = {3}>
                    <p>Health Score: 1.0</p>
                </Col>
            </Row>
            <Row className="text-left">
                <Col md = {3}>
                    <p>Servings: 8</p>
                </Col>
                <Col md = {3}>
                    <p>Summary:
                    Blueberry Rhubarb Pie is a dessert that serves 8. Watching your figure? This lacto ovo
                    vegetarian recipe has 317 calories, 4g of protein, and 12g of fat per serving.
                    $1.04 per serving, this recipe covers 6% of your daily requirements of vitamins and
                    minerals. 1 person has made this recipe and would make it again. Head to the store and
                    pick up salt, nutmeg, flour, and a few other things to make it today. It can be enjoyed
                    any time, but it is especially good for Mother's Day.</p>
                </Col>
                <Col>
                    <Image src="https://spoonacular.com/recipeImages/1003464-556x370.jpg" fluid />
                </Col>
            </Row>
        </Container>
    </div>

)
