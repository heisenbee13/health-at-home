import React, { Component } from 'react'
import {Posts_Recipes} from './Components/Posts_Recipes';
import { Button, Image, Col, Container, Row } from 'react-bootstrap'
import axios, { AxiosRequestConfig, AxiosPromise, AxiosResponse } from 'axios';
import {Pagination} from './Components/Pagination2';
import Table from 'react-bootstrap/Table';
import ButtonToolbar from 'react-bootstrap/esm/ButtonToolbar'
import { Link, useParams, RouteComponentProps } from 'react-router-dom'


export class Recipes extends Component {
        state = {
            posts: [], 
            loading: false, 
            currentPage: 1, 
            postsPerPage: 5
        };

        getPosts = async () => {
          this.setState({loading: true});
                const results = await axios.get('https://api.healthathome.me/api/recipes?page='.concat(this.state.currentPage.toString()));
                this.setState({posts: results.data, loading: false});
        };

        componentDidMount() {
          this.getPosts();
        }

        render() {
            const { currentPage, postsPerPage, posts, loading} = this.state;
            const paginate = pageNum => this.setState({currentPgae: pageNum})
            const nextPage = () => {
                this.state.currentPage = this.state.currentPage+1;
                this.getPosts();
            };
            const prevPage = () => {
                if (this.state.currentPage!=1)
                    this.state.currentPage = this.state.currentPage-1;
                this.getPosts();
            };

            return (
                <div className="container">
                    <h1 className = "my-5 text-primary text-center">Recipes</h1>
                    <hr></hr>
                    <h4>Take your pick of recipes that are not only healthy but also DELICIOUS.</h4>
                    <p>Find foods that you love, guilt-free snacking guaranteed!</p>
                    <hr></hr>

  <Table striped bordered hover>
  </Table>
        <Posts_Recipes posts = {posts}/>
        <Pagination currentPage={currentPage} nextPage={nextPage} prevPage={prevPage} />
        <Button variant="Primary" className="bg-primary text-white" href={'/workouts'}>View Workouts!</Button>
        <Button variant="Primary" className="bg-primary text-white" href={'/healthnews'}>View Health News!</Button>
        </div>
            )
        }

        

    }
   