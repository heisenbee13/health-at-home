import React from 'react'

export const Home = () => (
    <div>
    <h1>EMBRACE a healthy lifestyle today</h1>
        <br></br>
        <h2>Let us help you achieve your personal fitness and wellbeing goals!</h2>
        <br></br>
        <hr></hr>
        <h4>Find hundreds of delicious recipes, effective workouts, and the latest health articles!</h4>
        <br></br>
        <h5>Live your BEST life with Health At Home by your side.</h5>
        <hr></hr>
    </div>

)
