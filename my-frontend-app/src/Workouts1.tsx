import React, { Component } from 'react'
import {Posts} from './Components/Posts';
import axios, { AxiosRequestConfig, AxiosPromise, AxiosResponse } from 'axios';
import {Pagination} from './Components/Pagination1';
import { Link, useParams, RouteComponentProps } from 'react-router-dom'


export class Workouts1 extends Component {
        state = {
            posts: [], 
            loading: false, 
            currentPage: 1, 
            postsPerPage: 5
        };

        // componentDidMount() {
        //     const getPosts = async () => {
        //         this.setState({loading: true});
        //         const results = await axios.get("https://api.healthathome.me/api/workouts?page=".concat(this.state.currentPage.toString()));
        //         this.setState({posts: results.data});
        //         this.setState({loading: false});
        //     };
        //     getPosts();
        // }

         getPosts = async () => {
            this.setState({loading: true});
            const results = await axios.get("https://api.healthathome.me/api/workouts?page=".concat(this.state.currentPage.toString()));
            this.setState({posts: results.data, loading: false});
            
        };

        componentDidMount() {
            this.getPosts();
        }

        render() {
            const { currentPage, postsPerPage, posts, loading} = this.state;
            const paginate = pageNum => this.setState({currentPgae: pageNum})
            const nextPage = () => {
                this.state.currentPage = this.state.currentPage+1;
                this.getPosts();
            };
            const prevPage = () => {
                if (this.state.currentPage!=1)
                    this.state.currentPage = this.state.currentPage-1;
                this.getPosts();
            };
            // const indexOfLastPost = currentPage * postsPerPage;
            // const indexOfFirstPost = indexOfLastPost - postsPerPage;
            // const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);
            // const paginate = pageNum => this.setState({currentPage: pageNum})
            // const nextPage = () => this.setState({currentPage: currentPage + 1});
            // const prevPage = () => this.setState({currentPage: currentPage - 1});

            return (
                <div className="container">
                    <h1 className = "my-5 text-primary text-center">Workouts Draft</h1>
                    <Posts posts = {posts} loading={loading}/>
                    <Pagination currentPage={currentPage} nextPage={nextPage} prevPage={prevPage} />

                </div>
            )
        }

    }
   
