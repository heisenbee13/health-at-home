from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# very basic tests: if it loads, it should pass

driver = webdriver.Firefox()
driver.get("http://localhost:80/")
# assert "Health At Home" in driver.title
assert "Health At Home" in driver.page_source
assert "lifestyle" in driver.page_source
assert "No Match" not in driver.page_source

driver.get("http://localhost:80/recipes")
assert "Recipes" in driver.page_source
assert "Calories" in driver.page_source
assert "Ingredients" in driver.page_source
assert "Time" in driver.page_source
assert "Price" in driver.page_source

driver.get("http://localhost:80/workouts")
assert "Workouts" in driver.page_source
assert "No Match" not in driver.page_source

driver.get("http://localhost:80/healthnews")
assert "Health News" in driver.page_source
assert "No Match" not in driver.page_source

driver.get("http://localhost:80/about")
assert "Our Mission" in driver.page_source
assert "Our Team" in driver.page_source
assert "Tools and Documentation" in driver.page_source


driver.close()
