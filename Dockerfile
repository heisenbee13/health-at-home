# Lines 1-20: Setup image

FROM nikolaik/python-nodejs

RUN ls && pwd

#  !!!repo/branch depends on what thing you want to test inside docker.
# RUN git clone --branch develop https://gitlab.com/health-at-home-team/health-at-home.git
RUN git clone https://gitlab.com/health-at-home-team/health-at-home.git

WORKDIR /health-at-home
RUN git pull --force
RUN ls && pwd

# Update object-path due to security vulnerability in versions <0.11.5
RUN cd my-frontend-app && npm install && npm update object-path && npm run build && cd ..

RUN pip3 install -r backend/requirements.txt
# RUN npm install -g newman && npm install -g mocha

EXPOSE 80

# Lines 22-24: Run every time we start this container

CMD git pull --force && python3 backend/main.py
