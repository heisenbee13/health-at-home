from main import app
from main import GenericError, handleGenericError
from main import validateID
import unittest

class FlaskTest(unittest.TestCase):
    # Recpie Status Code
    def test_recipe_status(self):
        tester = app.test_client(self)
        response = tester.get('/api/recipes')
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    #Recipe Content type
    def test_recipe_content_type(self):
        tester = app.test_client(self)
        response = tester.get('/api/recipes')
        self.assertEqual(response.content_type, 'application/json')

    # Workout Status Code
    def test_workout_status(self):
        tester = app.test_client(self)
        response = tester.get('/api/workouts')
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    # Workout content type
    def test_workout_content_type(self):
        tester = app.test_client(self)
        response = tester.get('/api/workouts')
        self.assertEqual(response.content_type, 'application/json')

   # News Status Code
    def test_news_status(self):
        tester = app.test_client(self)
        response = tester.get('/api/healthnews')
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    # News content type
    def test_news_content_type(self):
        tester = app.test_client(self)
        response = tester.get('/api/healthnews')
        self.assertEqual(response.content_type, 'application/json')

    # Make sure arguments to error constructor work; trivial but can't test handler easily
    def test_error_constructor(self):
        error = GenericError("no fun allowed", 999, "test-payload")
        self.assertEqual(error.message, "no fun allowed")
        self.assertEqual(error.status_code, 999)
        self.assertEqual(error.payload, "test-payload")

    # make sure ID validation works as intended
    def test_id_validation(self):
        self.assertEqual(validateID(1234), True)
        self.assertEqual(validateID(1), True)
        self.assertEqual(validateID(0), False)
        self.assertEqual(validateID(-1), False)

if __name__ == '__main__':
    unittest.main()
