from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, String, Integer
from flask_cors import CORS
import requests
import time
import urllib
import os
import json
from sqlalchemy import create_engine
import flask_restless

basedir = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__, static_folder='../my-frontend-app/build/static',
            template_folder='../my-frontend-app/build')
CORS(app)

PAGESIZE = 20 # TODO this and other constants to file: constant.py?

# OK to define these in other files
ENV_TYPE = os.getenv('ENV_TYPE')
DB_URL   = 'healthathomedb.coacny3dggy8.us-east-1.rds.amazonaws.com' #os.getenv('DB_ADDRESS')
DB_PORT  = '5432' #os.getenv('DB_PORT')
DB_NAME  = 'postgres' #os.getenv('DB_NAME')

# Secret, real values as environment variables only
USERNAME = os.getenv('DB_USERNAME')
PASSWORD = os.getenv('DB_PASSWORD')
KEY_FOOD = os.getenv('API_KEY_FOOD')
KEY_NEWS = os.getenv('API_KEY_NEWS')
KEY_OURS = os.getenv('API_KEY_OURS')

app.config['SQLALCHEMY_DATABASE_URI']=f'postgresql+psycopg2://{USERNAME}:{PASSWORD}@{DB_URL}:{DB_PORT}/{DB_NAME}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)

#Recipe Model
class Recipe(db.Model):
    __tablename__ = 'recipes'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    cuisines = db.Column(db.PickleType())
    diets = db.Column(db.PickleType())
    pricePerServing = db.Column(db.Integer)
    readyInMinutes = db.Column(db.Integer)
    servings = db.Column(db.Integer)
    image = db.Column(db.String())
    summary = db.Column(db.String())
    nutrition = db.Column(db.PickleType())
    analyzedInstructions = db.Column(db.PickleType())
    imageType = db.Column(db.String())
    sourceUrl = db.Column(db.String())

class RecipeSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'cuisines', 'diets', 'pricePerServing',
             'readyInMinutes', 'servings', 'image', 'summary', 'nutrition', 'analyzedInstructions', 'imageType', 'sourceUrl') #, 'ingredients')

recipe_schema = RecipeSchema()
recipes_schema = RecipeSchema(many=True)

class Workout(db.Model):
    __tablename__ = 'workouts'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(), nullable = False)
    category = db.Column(db.String())
    equipment = db.Column(db.PickleType())
    muscles = db.Column(db.PickleType())
    muscles_secondary = db.Column(db.PickleType())
    description = db.Column(db.String())
    exercise_images = db.Column(db.PickleType())
    language = db.Column(db.String())
    license = db.Column(db.String())
    license_author = db.Column(db.String())

class WorkoutSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'category', 'equipment',
                  'muscles', 'muscles_secondary', 'description',
                  'exercise_images', 'language', 'license', 'license_author')

workout_schema = WorkoutSchema()
workouts_schema = WorkoutSchema(many = True)

#Article Model
class Article(db.Model):
    __tablename__ = 'articles'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String())
    author = db.Column(db.String())
    source = db.Column(db.String())
    description = db.Column(db.String())
    publishedAt = db.Column(db.String())
    content = db.Column(db.String())
    country = db.Column(db.String())
    url = db.Column(db.String())
    urlToImage = db.Column(db.String())

class ArticleSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'author', 'source', 'description', 'publishedAt',
                  'content',  'url', 'urlToImage')

article_schema = ArticleSchema()
articles_schema = ArticleSchema(many=True)

class GenericError(Exception) :
    status_code = 400
    def __init__ (self, message, status_code = None, payload = None) :
        Exception.__init__(self)
        self.message = message
        if status_code is not None :
            self.status_code = status_code
        self.payload = payload
    def to_dict(self) :
        if self.payload is None :
            self.payload = ()
        errDict = dict(self.payload)
        errDict['message'] = self.message
        return errDict

db.create_all() # This should run fine - checks for table existence first

@app.route('/', defaults={'path':''})
@app.route('/<path:path>')
def index(path):
    return render_template("index.html")

@app.errorhandler(GenericError)
def handleGenericError(error) :
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.route('/api/testScrape', methods=['POST'])
def testScraper() :
    authKey = request.args.get('authKey', '', type = str)
    if authKey != KEY_OURS :
        raise GenericError('Sorry, you do not have authorization for this. ' +
                           'If you have an auth key, make sure you entered it correctly.',
                           status_code = 401) # doesn't fit 401 perfectly but good enough for now
    type = request.args.get('type', 'No type provided', type = str)
    if type == 'recipe' :
        offset = request.args.get('offset', 0, type = int)
        response = scrapeRecipes(offset)
        # this response is constructed differently as a list of dictionaries containing desired properties
        for r in response :
            recipe = Recipe(**r)
            db.session.add(recipe)
        db.session.commit()
        return f'Completed offset range {offset + 1} to {offset + 10}'
    elif type == 'workout' :
        offset = request.args.get('offset', 0, type = int)
        response = scrapeWorkouts(offset)
        for r in response['results'] :
            workout = Workout(**r)
            db.session.add(workout)
        db.session.commit()
        return f'Finished range {offset + 1} to {offset + 20}. Next offset: {offset + 20}'
    elif type == 'article' :
        page = request.args.get('page', 1, type = int)
        response = scrapeArticles(page)
        for r in response['articles'] :
            r['source'] = r['source']['name']
            article = Article(**r)
            db.session.add(article)
        db.session.commit()
        return f'Completed page {page}'
    else:
        raise GenericError('Make sure to correctly specify the model you want to scrape.')

@app.route('/api/recipes', methods=['GET'])
def getRecipes():
    page = request.args.get('page', 1, type = int)
    query = Recipe.query.filter_by()
    recipes = query.paginate(page, PAGESIZE, False)
    response = jsonify(recipes_schema.dump(recipes.items))
    response.headers['total-pages'] = recipes.pages
    return response

@app.route('/api/recipeInfo', methods=['GET'])
def getRecipe() :
    id = request.args.get('id', -1, type = int)
    if validateID(id) :
        recipe = Recipe.query.filter_by(id = id).first_or_404(description=f'Could not find recipe with id {id}')
        response = recipes_schema.dump(recipe, many = False)
        return jsonify(response)
    else :
        raise GenericError('Please specify a valid ID (a positive integer)')

@app.route('/api/workouts', methods=['GET'])
def getWorkouts() :
    page = request.args.get('page', 1, type = int)
    query = Workout.query.filter_by(language = '2')
    workouts = query.paginate(page, PAGESIZE, False)
    response = jsonify(workouts_schema.dump(workouts.items))
    response.headers['total-pages'] = workouts.pages
    return response

@app.route('/api/workoutInfo', methods=['GET'])
def getWorkout() :
    id = request.args.get('id', -1, type = int)
    if validateID(id) :
        workout = Workout.query.filter_by(id = id).first_or_404(description=f'Could not find workout with id {id}')
        response = workouts_schema.dump(workout, many = False)
        return jsonify(response)
    else :
        raise GenericError('Please specify a valid ID (a positive integer)')

@app.route('/api/healthnews', methods=['GET'])
def getArticles() :
    page = request.args.get('page', 1, type = int)
    query = Article.query.filter_by()
    articles = query.paginate(page, PAGESIZE, False)
    response = jsonify(articles_schema.dump(articles.items))
    response.headers['total-pages'] = articles.pages
    return response

@app.route('/api/healthnewsInfo', methods=['GET'])
def getArticle() :
    id = request.args.get('id', -1, type = int)
    if validateID(id) :
        article = Article.query.filter_by(id = id).first_or_404(description=f'Could not find article with id {id}')
        response = article_schema.dump(article, many = False)
        return jsonify(response)
        # return article_schema.jsonify(article) # ???
    else :
        raise GenericError('Please specify a valid ID (a positive integer)')

def validateID(id) :
    return id > 0

def scrapeRecipes(offset) :
    recipe_params = {'addRecipeNutrition' : 'true', 'offset' : offset, 'addRecipeInformation': 'true', 'apiKey' : KEY_FOOD}
    raw = requests.get('https://api.spoonacular.com/recipes/complexSearch', params = recipe_params).json()
    response = []
    #Only take information we want from schema instead of removing fields we don't want
    for r in raw['results'] :
        recipe = {}
        for field in RecipeSchema.Meta.fields :
            recipe[field] = r[field]
        response.append(recipe)
    return response

def scrapeWorkouts(offset) : # how to handle source error/rate limit?
    workoutparams = {'status' : '2', 'offset' : offset}
    response = requests.get('https://wger.de/api/v2/exercise', params = workoutparams).json()
    for r in response['results'] :
        imgparams = {'exercise' : r['id'], 'status' : '2'}
        images = requests.get('https://wger.de/api/v2/exerciseimage', params = imgparams).json()
        r['exercise_images'] = images['results']
        del r['status']
        del r['name_original']
        del r['creation_date']
        del r['uuid']
        print('Prepared workout with id ' + str(r['id']))
        time.sleep(0.5)
    return response

def scrapeArticles(page) :
    articleparams = {'qInTitle' : 'nutrition', 'page' : page, 'apiKey' : KEY_NEWS}
    response = requests.get('http://newsapi.org/v2/everything', params = articleparams).json()
    return response

db.create_all()
manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)

#Run Server
if __name__ == '__main__':
    debug = False if ENV_TYPE == 'production' else True
    app.run(host='0.0.0.0', port=80, threaded=True, debug=debug)
